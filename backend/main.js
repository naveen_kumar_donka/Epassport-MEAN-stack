const express = require('express')
const app = express()
const cors = require('cors')
app.use(cors())
const bodyparser = require('body-parser')
app.use(bodyparser.urlencoded({ extended: true }))
app.use(bodyparser.json())

const routes = require('./routes')

//database connection 
const mongoose = require("mongoose")
const dbUrl = 'mongodb://localhost:27017/epassport'
mongoose.connect(dbUrl).then(()=>{
    console.log("connected to dabase epassport")
})
.catch((err)=>{
    console.log("error while connecting to databse",err);
})

app.use('/api',routes)
//basic api connected
app.get('/',(req,res)=>{
    res.send("welcome to epassport")
})


module.exports = app