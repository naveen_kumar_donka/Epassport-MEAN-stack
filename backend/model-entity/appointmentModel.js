const mongoose=require('mongoose')
const appointmentModel = new mongoose.Schema({
    MonthID:{
        type:String,
        require:true
    },
    MonthName:{
        type:String,
        require:true
    },
    AvailableDays:{
        type:Array,
        require:true
    },
    TimeSlots:{
        type:String,
        require:true
    }
})
const appointment=mongoose.model('appointment',appointmentModel)
module.exports=appointment