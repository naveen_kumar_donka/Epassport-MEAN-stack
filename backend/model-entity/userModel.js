const mongoose=require('mongoose')
let userSchema = new mongoose.Schema({
    Firstname:{
        type:String,
        require:true
    },
    Lastname:{
        type:String,
        require:true
    },
    DateofBirth:{
        type:String,
        require:true
    },
    Gender:{
        type:String,
        require:true
    },
    email:{
        type:String,
        require:true
    },
    password:{
        type:String,
        require:true
    },
    phoneNo:{
        type:String,
        require:true
    },
    passportStatus:{
        type:Number,
        require:true,
        default:0
    }
})

const user = mongoose.model('user',userSchema)
module.exports=user