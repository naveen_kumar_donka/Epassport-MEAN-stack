const mongoose=require('mongoose')
const officeDetailsModel = new mongoose.Schema({
    OfficeId:{
        type:String,
        require:true
    },
    OfficeName:{
        type:String,
        require:true
    },
    Jurisdiction:{
        type:String,
        require:true
    },
    Adress:{
        type:String,
        require:true
    },
    PhoneNumber:{
        type:String,
        require:true
    }
})
const officeDetails = mongoose.model('officedetails',officeDetailsModel)
module.exports=officeDetails