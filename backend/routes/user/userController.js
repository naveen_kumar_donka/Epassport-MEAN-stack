const router = require("express").Router()
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')
const userModel=require('../../model-entity/userModel');
const applicationModel=require('../../model-entity/applicationModel')
const appointmentModel = require('../../model-entity/appointmentModel')
const { json } = require("body-parser");
const auth = require('../../middleware')

router.post('/register',async(req,res)=>{
    var Password;
    Firstname=req.body.Firstname
    Lastname=req.body.Lastname
    DateofBirth=req.body.DateofBirth
    Gender=req.body.Gender
    email=req.body.email
    password=await bcrypt.hash(req.body.password,10)
    phoneNo=req.body.phoneNo
    let user={Firstname,Lastname,DateofBirth,Gender,email,password,phoneNo}
    userModel(user).save().then(()=>{
        res.status(200).send("data addeded")
    })
    .catch((e)=>{
        res.status(400).send("error",e)
    })
})

router.post('/login',async(req,res)=>{
    const user = await userModel.findOne({email : req.body.email})
    if(user)
    {
        bcrypt.compare(req.body.password,user.password,(err,result)=>{
            if(result){
                payload = {email:req.body.email}
                auth.Genaratetoken(payload,(err,token)=>{
                    res.send({username:"user login sucessfully",token:token})
                })         
            }
            else{
                res.send("invalid user")
            }
        })
    }
    else    
        res.status(400).send("invalid user")
})

router.get('/loaduser/:email',(req,res)=>{
    userModel.find({email: req.params.email}).then(result =>{
        res.status(200).json(result)
    })
    .catch(e=>{
        res.status(400).send("error while fetching the data",e)
    })
})

router.get('/viewschedule',(req,res)=>{
    appointmentModel.find()
    .then(result =>{
        res.status(200).json({schedules:result})
    })
    .catch(e=>{
        res.status(400).send("error while fetching the data",e)
    })
})
router.get('/viewapplicationform/:email',async(req,res)=>{
    applicationModel.find({emailID: req.params.email}).then(result =>{
        res.status(200).json(result)
    })
    .catch(e=>{
        res.status(400).send("error while fetching the data",e)
    })
})

router.post('/fillapplicationform',(req,res)=>{
    firstName=req.body.firstName
    lastName=req.body.lastName
    DOB=req.body.DOB
    placeOfBirth=req.body.placeOfBirth
    gender=req.body.gender
    state=req.body.state
    district=req.body.district
    maritalStatus=req.body.maritalStatus
    PAN=req.body.PAN
    employmentType=req.body.employmentType
    highestEducation=req.body.highestEducation
    fathersName=req.body.fathersName
    mothersName=req.body.mothersName
    houseNo=req.body.houseNo
    streetName=req.body.streetName
    city=req.body.city
    state1=req.body.state
    district1=req.body.district
    PINCode=req.body.PINCode
    telephoneNumber=req.body.telephoneNumber
    emailID=req.body.emailID
    referenceName=req.body.referenceName
    address=req.body.address
    RtelephoneNumber=req.body.telephoneNumber

    let application={firstName,lastName,DOB,placeOfBirth,gender,
        state,district,
        maritalStatus,PAN,employmentType,highestEducation,
        fathersName,mothersName,
        houseNo,streetName,
        city,state1,district1,
        PINCode,telephoneNumber,
        emailID,referenceName,
        address,RtelephoneNumber}
    applicationModel(application).save().then(()=>{
        res.status(200).send("application submitted sucessfully")
    })
    .catch((e)=>{
        res.status(400).send("error while submitting application")
    })
})
router.post('/bookappointment',(req,res)=>{
    MonthId=req.body.MonthId
    MonthName=req.body.MonthName
    AvailableDays=req.body.AvailableDays
    TimeSlots=req.body.TimeSlots
    let schedules = {MonthId,MonthName,AvailableDays,TimeSlots}
    appointmentModel(schedules).save().then(()=>{
        res.status(200).send("schedule booked")
    })
    .catch((e)=>{
        res.status(400).send("errror while adding the schedule",e)
    })
})

//logout
router.get('/logout',(req,res)=>{
    res.status(200).send("user logout sucessfully")
})

module.exports=router