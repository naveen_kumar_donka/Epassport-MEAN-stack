const router = require('express').Router()
const officeDetailsModel = require('../../model-entity/officeDetailsModel')
const userModel = require('../../model-entity/userModel')
const bcrypt = require('bcrypt')
const mongoose = require('mongoose')
const appointmentModel = require('../../model-entity/appointmentModel')
const adminName='admin@gmail.com',adminPassword="Admin@123";
const applicationModel = require('../../model-entity/applicationModel')

router.post('/login',(req,res)=>{
    email=req.body.email,
    password=req.body.password
    if(email==adminName && password==adminPassword){
        return res.status(200).send("admin login sucessfully")
    }
    else{
        return res.status(400).send("invalid login and password")
    }
})

//data adding 
router.post('/addofficedetails',(req,res)=>{
    OfficeId=req.body.OfficeId
    OfficeName=req.body.OfficeName
    Jurisdiction=req.body.Jurisdiction
    Adress=req.body.Adress
    PhoneNumber=req.body.PhoneNumber
    let officedetails= {OfficeId,OfficeName,Jurisdiction,Adress,PhoneNumber}
    officeDetailsModel(officedetails).save().then(()=>{
        res.status(200).send("office data added sucessfully by admin")
    }).catch((e)=>{
        res.status(400).send("error while adding the office details",e)
    })
})
router.post('/adduser',async(req,res)=>{
    var Password;
    Firstname=req.body.Firstname
    Lastname=req.body.Lastname
    DateofBirth=req.body.DateofBirth
    Gender=req.body.Gender
    email=req.body.email
    password=await bcrypt.hash(req.body.password,10)
    phoneNo=req.body.phoneNo
    let user={Firstname,Lastname,DateofBirth,Gender,email,password,phoneNo}
    userModel(user).save().then(()=>{
        res.status(200).send("user added sucessfully by admin")
    })
    .catch((e)=>{
        res.status(400).send("error",e)
    })
})
router.post('/addschedule',(req,res)=>{
    MonthID=req.body.MonthID
    MonthName=req.body.MonthName
    AvailableDays=req.body.AvailableDays
    TimeSlots=req.body.TimeSlots
    let schedules = {MonthID,MonthName,AvailableDays,TimeSlots}
    appointmentModel(schedules).save().then(()=>{
        res.status(200).send("appointment added")
    })
    .catch((e)=>{
        res.status(400).send("errror while adding the schedule",e)
    })
})

// fetch data.. 
router.get('/users',(req,res)=>{
    userModel.find()
    .then(result =>{
        res.status(200).json(result)
    })
    .catch(e=>{
        res.status(400).send("error while fetching the data",e)
    })
})
router.get('/officedetails',(req,res)=>{
    officeDetailsModel.find()
    .then(result =>{
        res.status(200).json(result)
    })
    .catch(e=>{
        res.status(400).send("error while fetching the data",e)
    })
})
router.get('/schedules',(req,res)=>{
    appointmentModel.find()
    .then(result =>{
        res.status(200).json(result)
    })
    .catch(e=>{
        res.status(400).send("error while fetching the data",e)
    })
})
router.get('/applications',(req,res)=>{
    applicationModel.find().then(result=>{
        res.status(200).json(result)
        // console.log(result);
    })
    .catch(e=>{
        res.status(400).send('error',e)
    })
})

//find single item By id

router.get('/user/:id',(req,res)=>{
    userModel.find({_id:req.params.id})
    .then((user)=>{
        res.status(200).json(user)
    })
    .catch(err=>{
        res.status(400).send(err)
    })
})


router.get('/oneschedule/:id',(req,res)=>{
    appointmentModel.find({_id:req.params.id})
    .then((schedule)=>{
        res.status(200).json(schedule)
    })
    .catch(err=>{
        res.status(400).send(err)
    })
})

router.get('/oneoffice/:id',(req,res)=>{
    officeDetailsModel.find({_id:req.params.id})
    .then((office)=>{
        res.status(200).json(office)
    })
    .catch(err=>{
        res.status(400).send(err)
    })
})
router.get('/oneApplicationdata/:id',(req,res)=>{
    applicationModel.findById({_id:req.params.id})
    .then(result=>{
        res.status(200).json(result)
    })
})

//update data 
router.put('/updateuser/:id',async(req,res)=>{
        Firstname=req.body.Firstname
        Lastname=req.body.Lastname
        DateofBirth=req.body.DateofBirth
        Gender=req.body.Gender
        email=req.body.email
        password=req.body.password
        phoneNo=req.body.phoneNo
        passportStatus=req.body.passportStatus
        let updateUser ={Firstname,Lastname,DateofBirth,Gender,email,password,phoneNo,passportStatus}
        userModel.findOneAndUpdate({_id:req.params.id},updateUser)
        .then(()=>{
            res.status(200).send("user updated");
        })
        .catch((e)=>{
            res.status(400).send('error',e)
        })
})
router.put('/updateofficedetails/:id',(req,res)=>{
    OfficeId=req.body.OfficeId
    OfficeName=req.body.OfficeName
    Jurisdiction=req.body.Jurisdiction
    Adress=req.body.Adress
    PhoneNumber=req.body.PhoneNumber
    let officedetails= {OfficeId,OfficeName,Jurisdiction,Adress,PhoneNumber}
    officeDetailsModel.findOneAndUpdate({_id:req.params.id},officedetails)
    .then(()=>{
        res.status(200).send("schedule updated");
    })
    .catch((e)=>{
        res.status(400).send('error',e)
    })
})
router.put('/updateschedule/:id',(req,res)=>{
    MonthID=req.body.MonthID
    MonthName=req.body.MonthName
    AvailableDays=req.body.AvailableDays
    TimeSlots=req.body.TimeSlots
    let schedules = {MonthID,MonthName,AvailableDays,TimeSlots}
    appointmentModel.findOneAndUpdate({_id:req.params.id},schedules)
    .then(()=>{
        res.status(200).send("schedule updated");
    })
    .catch((e)=>{
        res.status(400).send('error',e)
    })
})

//delete data
router.delete('/deleteuser/:id',(req,res)=>{
    userModel.findByIdAndDelete(req.params.id)
    .then(user=>{
        if(user){
            res.send("user delete ")
        }
    })
    .catch(e=>{
        res.send("error while deleting",e)
    })
})
router.delete('/deleteoffice/:id',(req,res)=>{
    officeDetailsModel.findByIdAndDelete(req.params.id)
    .then(office=>{
        if(office){
            res.send("office deleted")
        }
    })
})
router.delete('/deleteschedule/:id',async (req,res)=>{
    appointmentModel.findByIdAndRemove(req.params.id)
    .then(schedule=>{
       if(schedule){
        res.send("deleted the appointment schendule")
       }
    })
    .catch(e=>{
        res.send("error while deleting",e)
    })
})
router.delete('/deleteapplication/:id',(req,res)=>{
    applicationModel.findByIdAndDelete({_id:req.params.id})
    .then(application=>{
        if(application){
            res.status(200).send("deleted the appliaction by admin")
        }
    })
})

//logout
router.get('/logout',(req,res)=>{
    res.status(200).send("admin logout sucessfully")
})

module.exports=router