const jwt = require('jsonwebtoken')
const secret = require('./authSecret')

const Genaratetoken = (payload,done) =>{ 
    jwt.sign(payload,secret.authConfig.jwtSecret, { expiresIn: '1h' },done)
}
const validateToken = (req,res,next)=>{
    let {authorization}=req.headers
    if(!authorization){
        res.status(401).send("token is not available")
    }
    let token = authorization.replace("bearer","")
    jwt.verify(token,secret.authConfig.jwtSecret,(err,payload)=>{
        if(err){
            res.send("must be login")
        }
        const _id=payload
        console.log(_id);
    })
}

module.exports={Genaratetoken,validateToken}