import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DefaultComponent } from './user/default/default.component';
import { LoginComponent } from './user/login/login.component';
import { RegisterComponent } from './user/register/register.component';
import { DashboardComponent } from './user/dashboard/dashboard.component';
import { PassportofficesComponent } from './user/passportoffices/passportoffices.component';
import { ApplicationformComponent } from './user/applicationform/applicationform.component';
import { ViewapplicationComponent } from './user/viewapplication/viewapplication.component';
import { BookscheduleComponent } from './user/bookschedule/bookschedule.component';
import { AdminloginComponent } from './admin/adminlogin/adminlogin.component';
import { ViewusersComponent } from './admin/viewusers/viewusers.component';
import { ViewofficesComponent } from './admin/viewoffices/viewoffices.component';
import { ViewoscheduleComponent } from './admin/viewoschedule/viewoschedule.component';
import { AdduserComponent } from './admin/adduser/adduser.component';
import { AddofficeComponent } from './admin/addoffice/addoffice.component';
import { AddScheduleComponent } from './admin/add-schedule/add-schedule.component';
import { AdminDashboardComponent } from './admin/admin-dashboard/admin-dashboard.component';
import { GuardGuard } from './guard/guard.guard';
import { PassportstausComponent } from './user/passportstaus/passportstaus.component';
import { UserUpdateFormComponent } from './admin/viewusers/user-update-form/user-update-form.component';
import { OfficeUpdateFormComponent } from './admin/viewoffices/office-update-form/office-update-form.component';
import { ScheduleUpdateFormComponent } from './admin/viewoschedule/schedule-update-form/schedule-update-form.component';
import { AdminAuthGuard } from './guard/admin-auth.guard';
import { ViewfilledapplicationsComponent } from './admin/viewfilledapplications/viewfilledapplications.component';

const routes: Routes = [
  {
    path:'',
    component:DefaultComponent
  },
  {
    path:'login',
    component:LoginComponent
  },
  {
    path:'register',
    component:RegisterComponent
  },
  {
    path:'dashboard',
    component:DashboardComponent,
    canActivate:[GuardGuard]
  },
  {
    path:'viewpassportoffice',
    component:PassportofficesComponent,
    canActivate:[GuardGuard]
  },
  {
    path:'fillform',
    component:ApplicationformComponent,
    canActivate:[GuardGuard]
  },
  {
    path:'viewapplication',
    component:ViewapplicationComponent,
    canActivate:[GuardGuard]
  },
  {
    path:'bookschedule',
    component:BookscheduleComponent,
    canActivate:[GuardGuard]
  },
  {
    path:'passportstatus',
    component:PassportstausComponent,
    canActivate:[GuardGuard]
  },
  {
    path:'adminlogin',
    component:AdminloginComponent
  },
  {
    path:'admindashboard',
    component:AdminDashboardComponent,
    canActivate:[AdminAuthGuard]
  },
  {
    path:'viewusers',
    component:ViewusersComponent,
    canActivate:[AdminAuthGuard]
  },
  {
    path:'adduser',
    component:AdduserComponent,
    canActivate:[AdminAuthGuard]
  },
  {
    path:'viewoffices',
    component:ViewofficesComponent,
    canActivate:[AdminAuthGuard]
  },
  {
    path:'addoffice',
    component:AddofficeComponent,
    canActivate:[AdminAuthGuard]
  },
  {
    path:'viewschedules',
    component:ViewoscheduleComponent,
    canActivate:[AdminAuthGuard]
  },
  {
    path:'addschedule',
    component:AddScheduleComponent,
    canActivate:[AdminAuthGuard]
  },
  {
    path:'updateuser/:id',
    component:UserUpdateFormComponent,
    canActivate:[AdminAuthGuard]
  },
  {
    path:'updateoffices/:id',
    component:OfficeUpdateFormComponent,
    canActivate:[AdminAuthGuard]
  },
  {
    path:'updateschedule/:id',
    component:ScheduleUpdateFormComponent,
    canActivate:[AdminAuthGuard]
  },
  {
    path:'viewapplicationfilled',
    component:ViewfilledapplicationsComponent,
    canActivate:[AdminAuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
