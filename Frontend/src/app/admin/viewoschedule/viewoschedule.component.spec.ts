import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewoscheduleComponent } from './viewoschedule.component';

describe('ViewoscheduleComponent', () => {
  let component: ViewoscheduleComponent;
  let fixture: ComponentFixture<ViewoscheduleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ViewoscheduleComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewoscheduleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
