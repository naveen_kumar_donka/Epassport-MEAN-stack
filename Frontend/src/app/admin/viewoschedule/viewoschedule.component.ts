import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { schedule } from 'src/app/model/schedule/schedule.module';
import { ServiceService } from 'src/app/service/service.service';

@Component({
  selector: 'app-viewoschedule',
  templateUrl: './viewoschedule.component.html',
  styleUrls: ['./viewoschedule.component.css']
})
export class ViewoscheduleComponent implements OnInit {

  constructor(private scheduleService:ServiceService,private httpClient:HttpClient,private router:Router) { }
  data1:schedule[]=[]

  viewSchedule(){
    this.scheduleService.getSchedule().subscribe(data=>{
      this.data1=data      
    })
  }

  deleteSchedule(id:string){
    this.httpClient.delete(`http://localhost:3000/api/admin/deleteschedule/${id}`,{responseType:'text'}).subscribe(()=>{
      this.viewSchedule()
      alert("schedule is deleted sucessfully")
    })
  }
  updateSchedule(id:String){
    this.router.navigateByUrl(`/updateschedule/${id}`)
  }
  ngOnInit(): void {
    this.viewSchedule()
  }

}
