import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ServiceService } from 'src/app/service/service.service';

@Component({
  selector: 'app-schedule-update-form',
  templateUrl: './schedule-update-form.component.html',
  styleUrls: ['./schedule-update-form.component.css']
})
export class ScheduleUpdateFormComponent implements OnInit {

  updateScheduleForm = new FormGroup({
    MonthID:new FormControl(),
    MonthName:new FormControl(),
    AvailableDays:new FormControl(),
    TimeSlots:new FormControl()
  })
  UpdateSchedule()
  {
    let bodyData={
      MonthID:this.updateScheduleForm.value.MonthID,
      MonthName:this.updateScheduleForm.value.MonthName,
      AvailableDays:this.updateScheduleForm.value.AvailableDays,
      TimeSlots:this.updateScheduleForm.value.TimeSlots
    }
    let data=this.route.snapshot.params['id']
    this.httpClient.put(`http://localhost:3000/api/admin/updateschedule/${data}`,bodyData,{responseType:'text'}).subscribe(()=>{
      alert("schedule is updated sucessfully by admin")
      this.router.navigate(['/viewschedules'])
    })
  }
  constructor(private scheduleService:ServiceService,private httpClient:HttpClient,private route:ActivatedRoute,private router:Router) { }

  ngOnInit(): void {
    let data=this.route.snapshot.params['id']
    this.scheduleService.getOneSchedule(data).subscribe((result)=>{
      this.updateScheduleForm.controls['MonthID'].setValue(result[0].MonthID)
      this.updateScheduleForm.controls['MonthName'].setValue(result[0].MonthName)
      this.updateScheduleForm.controls['AvailableDays'].setValue(result[0].AvailableDays)
      this.updateScheduleForm.controls['TimeSlots'].setValue(result[0].TimeSlots)
    })
  }

}
