import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, CanActivate, Router } from '@angular/router';
import { request } from 'http';
import { users } from 'src/app/model/user/user.module';
import { ServiceService } from 'src/app/service/service.service';

@Component({
  selector: 'app-user-update-form',
  templateUrl: './user-update-form.component.html',
  styleUrls: ['./user-update-form.component.css']
})
export class UserUpdateFormComponent implements OnInit {

  password:String=''

  updateForm =  new FormGroup({
    Firstname:new FormControl(),
    Lastname:new FormControl(),
    DateofBirth:new FormControl(),
    Gender:new FormControl(),
    email:new FormControl(),
    password:new FormControl(),
    phoneNo:new FormControl(),
    passportStatus:new FormControl()
    })
  constructor(private httpClient:HttpClient,private userService:ServiceService,private router:Router,private route:ActivatedRoute) { }
    updateUser(){
      let bodyData ={
        Firstname:this.updateForm.value.Firstname,
        Lastname:this.updateForm.value.Lastname,
        DateofBirth:this.updateForm.value.DateofBirth,
        Gender:this.updateForm.value.Gender,
        email:this.updateForm.value.email,
        password:this.password,
        phoneNo:this.updateForm.value.phoneNo,
        passportStatus:this.updateForm.value.passportStatus
      }
      let data =this.route.snapshot.params['id']
      if(bodyData.passportStatus == 0 || bodyData.passportStatus == 1){
      this.httpClient.put(`http://localhost:3000/api/admin/updateuser/${data}`,bodyData,{ responseType: 'text' }).subscribe(()=>{
        alert('user updated by admin sucessfully')
        this.router.navigate(['/viewusers'])
      })
    }
    else{
      alert("passport status shoud be 0 or 1")
    } 
    }
  ngOnInit(): void {
    let data =this.route.snapshot.params['id']
    this.userService.getOneUser(data).subscribe((result)=>{
      this.updateForm.controls['Firstname'].setValue(result[0].Firstname);
      this.updateForm.controls['Lastname'].setValue(result[0].Lastname);
      this.updateForm.controls['DateofBirth'].setValue(result[0].DateofBirth);
      this.updateForm.controls['Gender'].setValue(result[0].Gender);
      this.updateForm.controls['email'].setValue(result[0].email);
      this.password=result[0].password
      this.updateForm.controls['phoneNo'].setValue(result[0].phoneNo);
      this.updateForm.controls['passportStatus'].setValue(result[0].passportStatus);
    })

}
}