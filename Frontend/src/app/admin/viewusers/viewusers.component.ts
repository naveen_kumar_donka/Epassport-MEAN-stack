import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { users } from 'src/app/model/user/user.module';
import { ServiceService } from 'src/app/service/service.service';

@Component({
  selector: 'app-viewusers',
  templateUrl: './viewusers.component.html',
  styleUrls: ['./viewusers.component.css']
})
export class ViewusersComponent implements OnInit {
  
  constructor(private userService:ServiceService,private httpClient:HttpClient,private router:Router) { }
  data1:users[]=[]
  ngOnInit(): void {
    this.viewUsers()    
  }
  viewUsers(){
    this.userService.getUsers().subscribe(data=>{
      this.data1=data
    })
  }
  deleteUser(id:string){
      this.httpClient.delete(`http://localhost:3000/api/admin/deleteuser/${id}`,{responseType:'text'}).subscribe(()=>{
      this.viewUsers()
      alert("user deleted by admin")
    })
  }
  updateUser(id:String){
    this.router.navigateByUrl(`/updateuser/${id}`)
  }
}
