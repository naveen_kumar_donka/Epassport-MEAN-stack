import { Component, OnInit } from '@angular/core';
import {FormsModule,ReactiveFormsModule} from '@angular/forms';
import { FormControl,FormGroup,Validators } from '@angular/forms';
import { HttpClient, HttpErrorResponse} from '@angular/common/http'
import { Router } from '@angular/router';

@Component({
  selector: 'app-adminlogin',
  templateUrl: './adminlogin.component.html',
  styleUrls: ['./adminlogin.component.css']
})
export class AdminloginComponent implements OnInit {
  constructor(private HttpClient:HttpClient,private router:Router){}
  loginForm = new FormGroup({
    email:new FormControl("",[Validators.required,Validators.minLength(3)]),
    password:new FormControl("",[Validators.required,Validators.minLength(6)])
  })
  email(){
    return this.loginForm.get('email')
  }
  password(){
    return this.loginForm.get('password')
  }

  NameError()
  {
    if(this.email()?.invalid && (this.email()?.dirty||this.email()?.touched)){
      if(this.email()?.hasError('required'))
    {
      return "email must be filled"
    }
    else if (this.email()?.hasError('minlength'))
    {
      return "email canot be lessthen 3 charcter"
    }
    else{
      return ""
    }
    }
    else{
      return "";
    }
  }

  PasswordError()
  {
    if(this.password()?.invalid && (this.password()?.dirty||this.password()?.touched)){
      if(this.password()?.hasError('required'))
      {
        return "password must be filled"
      }
      else if (this.password()?.hasError('minlength'))
      {
        return "password canot be lessthen 6 charcter"
      }
      else{
        return ""
      }
      }
      else{
        return "";
      }
    }
    login(){
      let bodyData={
        email:this.loginForm.value.email,
        password:this.loginForm.value.password
      }
      this.HttpClient.post('http://localhost:3000/api/admin/login',bodyData,{ responseType: 'text' }).subscribe((res:any)=>{
        alert("admin login done")
        this.router.navigate(['/admindashboard'])
        let admintoken=localStorage.setItem('admintoken',bodyData.email)
      })
    }
    ngOnInit(): void {
    }
}