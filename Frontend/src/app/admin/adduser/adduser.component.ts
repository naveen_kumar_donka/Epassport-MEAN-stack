import { Component, OnInit } from '@angular/core';
import {FormsModule,ReactiveFormsModule} from '@angular/forms';
import { FormControl,FormGroup,Validators } from '@angular/forms';
import { HttpClient} from '@angular/common/http'
import { Router } from '@angular/router';

@Component({
  selector: 'app-adduser',
  templateUrl: './adduser.component.html',
  styleUrls: ['./adduser.component.css']
})
export class AdduserComponent implements OnInit {

  constructor(private HttpClient:HttpClient,private router:Router){}
  registerForm = new FormGroup({
    Firstname:new FormControl("",[Validators.required,Validators.minLength(3)]),
    Lastname:new FormControl("",[Validators.required,Validators.minLength(3)]),
    DateofBirth:new FormControl("",[Validators.required,Validators.minLength(3)]),
    Gender:new FormControl("",[Validators.required,Validators.minLength(3)]),
    email:new FormControl("",[Validators.required,Validators.email]),
    password:new FormControl("",[Validators.required,Validators.minLength(6)]),
    phoneNo:new FormControl("",[Validators.required,Validators.pattern("^((\\+91-?)|0)?[0-9]{10}$")])
    })

  addUser(){
    let bodyData ={
      Firstname:this.registerForm.value.Firstname,
      Lastname:this.registerForm.value.Lastname,
      DateofBirth:this.registerForm.value.DateofBirth,
      Gender:this.registerForm.value.Gender,
      email:this.registerForm.value.email,
      password:this.registerForm.value.password,
      phoneNo:this.registerForm.value.phoneNo
    }
    if(bodyData.Firstname.length !=0 &&
       bodyData.Lastname.length !=0 && 
       bodyData.DateofBirth.length !=0 &&
       bodyData.Gender.length !=0 &&
       bodyData.email.length !=0 &&
       bodyData.phoneNo.length !=0 &&
       bodyData.password.length !=0){
    this.HttpClient.post('http://localhost:3000/api/user/register',bodyData,{ responseType: 'text' }).subscribe((res:any)=>{
      alert("user added sucessfully by admin")
      this.registerForm.reset()
      this.router.navigate(['/viewoffices'])
    })}
    else{
      alert("fill everything")
    }
  }
  ngOnInit(): void {
  }
}