import { Component, OnInit } from '@angular/core';
import {FormsModule,ReactiveFormsModule} from '@angular/forms';
import { FormControl,FormGroup,Validators } from '@angular/forms';
import { HttpClient} from '@angular/common/http'
import { Router } from '@angular/router';

@Component({
  selector: 'app-addoffice',
  templateUrl: './addoffice.component.html',
  styleUrls: ['./addoffice.component.css']
})
export class AddofficeComponent implements OnInit {
  constructor(private HttpClient:HttpClient,private router:Router){}
  addofficeForm=new FormGroup({
    OfficeId:new FormControl("",Validators.required),
    OfficeName:new FormControl("",Validators.required),
    Jurisdiction:new FormControl("",Validators.required),
    Adress:new FormControl("",Validators.required),
    PhoneNumber:new FormControl("",Validators.required)
  })

  addOffice(){
    
    let bodyData={
      OfficeId:this.addofficeForm.value.OfficeId,
      OfficeName:this.addofficeForm.value.OfficeName,
      Jurisdiction:this.addofficeForm.value.Jurisdiction,
      Adress:this.addofficeForm.value.Adress,
      PhoneNumber:this.addofficeForm.value.PhoneNumber
    }
    if(bodyData.OfficeId.length !=0 &&
      bodyData.OfficeName.length !=0 &&
      bodyData.Jurisdiction.length !=0 &&
      bodyData.Adress.length !=0 &&
      bodyData.PhoneNumber.length !=0 ){
          this.HttpClient.post('http://localhost:3000/api/admin/addofficedetails',bodyData,{ responseType: 'text' }).subscribe((res:any)=>{
              alert('office added by admin')
              this.router.navigate(['/viewoffices'])
              this.addofficeForm.reset()
          })
  }
  else{
    alert("fill the details")
  }
}

  ngOnInit(): void {
  }

}
