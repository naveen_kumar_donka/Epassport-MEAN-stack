import {FormsModule,ReactiveFormsModule} from '@angular/forms';
import { FormControl,FormGroup,Validators } from '@angular/forms';
import { HttpClient} from '@angular/common/http'
import { Router } from '@angular/router';import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-add-schedule',
  templateUrl: './add-schedule.component.html',
  styleUrls: ['./add-schedule.component.css']
})
export class AddScheduleComponent implements OnInit {
  constructor(private HttpClient:HttpClient,private router:Router){}
  addScheduleForm = new FormGroup({
    MonthID:new FormControl("",Validators.required),
    MonthName:new FormControl("",Validators.required),
    AvailableDays:new FormControl("",Validators.required),
    TimeSlots:new FormControl("",Validators.required)
  })

  addSchedule(){
    let bodyData ={
      MonthID:this.addScheduleForm.value.MonthID,
      MonthName:this.addScheduleForm.value.MonthName,
      AvailableDays:this.addScheduleForm.value.AvailableDays,
      TimeSlots:this.addScheduleForm.value.TimeSlots
    }
    if(bodyData.MonthID !=0 && bodyData.MonthName !=0 && bodyData.AvailableDays !=0 && bodyData.TimeSlots!=0){
      this.HttpClient.post('http://localhost:3000/api/admin/addschedule',bodyData,{ responseType: 'text' }).subscribe((res:any)=>{
        alert('schedule added by admin')
        this.addScheduleForm.reset()
        this.router.navigate(['/viewschedules'])
    })
    }
    else{
      alert("fill everything")
    }
  }
  ngOnInit(): void {
  }

}
