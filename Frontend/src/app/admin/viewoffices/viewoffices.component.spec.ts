import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewofficesComponent } from './viewoffices.component';

describe('ViewofficesComponent', () => {
  let component: ViewofficesComponent;
  let fixture: ComponentFixture<ViewofficesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ViewofficesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewofficesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
