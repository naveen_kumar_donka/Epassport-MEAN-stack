import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormGroup,FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ServiceService } from 'src/app/service/service.service';

@Component({
  selector: 'app-office-update-form',
  templateUrl: './office-update-form.component.html',
  styleUrls: ['./office-update-form.component.css']
})
export class OfficeUpdateFormComponent implements OnInit {

  updateOfficeForm = new FormGroup({
    OfficeId:new FormControl(),
    OfficeName:new FormControl(),
    Jurisdiction:new FormControl(),
    Adress:new FormControl(),
    PhoneNumber:new FormControl()
  })
  constructor(private officeService:ServiceService,private route:ActivatedRoute,private httpClient:HttpClient,private router:Router) { }
  updateOffice(){
    let bodyData={
      OfficeId:this.updateOfficeForm.value.OfficeId,
      OfficeName:this.updateOfficeForm.value.OfficeName,
      Jurisdiction:this.updateOfficeForm.value.Jurisdiction,
      Adress:this.updateOfficeForm.value.Adress,
      PhoneNumber:this.updateOfficeForm.value.PhoneNumber
    }
    let data=this.route.snapshot.params['id']
    this.httpClient.put(`http://localhost:3000/api/admin/updateofficedetails/${data}`,bodyData,{ responseType: 'text' }).subscribe(()=>{
      alert("office updated sucessfully by admin")
      this.router.navigate(['/viewoffices'])
    })
  }
  ngOnInit(): void {
    let data=this.route.snapshot.params['id']
    this.officeService.getOneOffice(data).subscribe((result)=>{
      this.updateOfficeForm.controls['OfficeId'].setValue(result[0].OfficeId)
      this.updateOfficeForm.controls['OfficeName'].setValue(result[0].OfficeName)
      this.updateOfficeForm.controls['Jurisdiction'].setValue(result[0].Jurisdiction)
      this.updateOfficeForm.controls['Adress'].setValue(result[0].Adress)
      this.updateOfficeForm.controls['PhoneNumber'].setValue(result[0].PhoneNumber)
    })
  }

}
