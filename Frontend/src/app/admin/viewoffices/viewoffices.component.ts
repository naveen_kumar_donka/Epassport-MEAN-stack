import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { office } from 'src/app/model/office/office.module';
import { ServiceService } from 'src/app/service/service.service';

@Component({
  selector: 'app-viewoffices',
  templateUrl: './viewoffices.component.html',
  styleUrls: ['./viewoffices.component.css']
})
export class ViewofficesComponent implements OnInit {
  constructor(private router:Router,private officeservice:ServiceService,private httpClient:HttpClient) { }
  viewOffices(){
    this.officeservice.getOffice().subscribe(data=>{
      this.data1=data
    })
  }
  deleteOffice(id:String){
    this.httpClient.delete(`http://localhost:3000/api/admin/deleteoffice/${id}`,{responseType:'text'}).subscribe(()=>{
      this.viewOffices()
      alert("office deleted by admin")
    })
  }
  updateOffice(id:String){
    this.router.navigateByUrl(`/updateoffices/${id}`)
  }
  data1:office[]=[]
  
  ngOnInit(): void {
    this.viewOffices()
}
}
