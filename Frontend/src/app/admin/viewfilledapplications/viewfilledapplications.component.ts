import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { applicationform } from 'src/app/model/application/application.module';
import { ServiceService } from 'src/app/service/service.service';

@Component({
  selector: 'app-viewfilledapplications',
  templateUrl: './viewfilledapplications.component.html',
  styleUrls: ['./viewfilledapplications.component.css']
})
export class ViewfilledapplicationsComponent implements OnInit {


  data1:applicationform[]=[]
  viewApplication(){
    this.ApplicationService.getAllApplications().subscribe(ress=>{
      this.data1=ress
    })
  }

  constructor(private ApplicationService:ServiceService,private httpClient:HttpClient,private router:Router) { }
  deleteApplication(id:String){
    this.httpClient.delete(`http://localhost:3000/api/admin/deleteapplication/${id}`,{responseType:'text'}).subscribe(()=>{
      this.viewApplication()
      alert("application deleted by admin")
    })
  }
  ngOnInit(): void {
    this.viewApplication()
  }
}
