import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewfilledapplicationsComponent } from './viewfilledapplications.component';

describe('ViewfilledapplicationsComponent', () => {
  let component: ViewfilledapplicationsComponent;
  let fixture: ComponentFixture<ViewfilledapplicationsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ViewfilledapplicationsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewfilledapplicationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
