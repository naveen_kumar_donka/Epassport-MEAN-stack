import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PassportstausComponent } from './passportstaus.component';

describe('PassportstausComponent', () => {
  let component: PassportstausComponent;
  let fixture: ComponentFixture<PassportstausComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PassportstausComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PassportstausComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
