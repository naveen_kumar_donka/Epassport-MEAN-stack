import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { users } from 'src/app/model/user/user.module';
import { ServiceService } from 'src/app/service/service.service';

@Component({
  selector: 'app-passportstaus',
  templateUrl: './passportstaus.component.html',
  styleUrls: ['./passportstaus.component.css']
})
export class PassportstausComponent implements OnInit {

  data1:users[]=[] 
  pStatus :any =''

  constructor(private userservice:ServiceService) { }

  ngOnInit(): void {
    let email:any= localStorage.getItem('token')
    this.userservice.loaduser(email).subscribe(ress=>{
      this.data1=ress
      if(this.data1[0].passportStatus == 1){
        this.pStatus='Approved'
      }
      else{
        this.pStatus = 'not approved'
      }
    })
  }
}
