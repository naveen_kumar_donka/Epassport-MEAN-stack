import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-applicationform',
  templateUrl: './applicationform.component.html',
  styleUrls: ['./applicationform.component.css']
})
export class ApplicationformComponent implements OnInit {

  localdata=localStorage.getItem('token')
  
  isDisabled:boolean = false

  applicationForm = new FormGroup({
    firstName:new FormControl("",[Validators.required,Validators.minLength(3)]),
    lastName:new FormControl("",[Validators.required,Validators.minLength(3)]),
    DOB:new FormControl("",[Validators.required,Validators.minLength(3)]),
    placeOfBirth:new FormControl("",[Validators.required,Validators.minLength(3)]),
    gender:new FormControl("",[Validators.required,Validators.minLength(3)]),
    state:new FormControl("",[Validators.required,Validators.minLength(3)]),
    district:new FormControl("",[Validators.required,Validators.minLength(3)]),
    maritalStatus:new FormControl("",[Validators.required,Validators.minLength(3)]),
    PAN:new FormControl("",[Validators.required,Validators.minLength(3)]),
    employmentType:new FormControl("",[Validators.required,Validators.minLength(3)]),
    highestEducation:new FormControl("",[Validators.required,Validators.minLength(3)]),
    fathersName:new FormControl("",[Validators.required,Validators.minLength(3)]),
    mothersName:new FormControl("",[Validators.required,Validators.minLength(3)]),
    houseNo:new FormControl("",[Validators.required,Validators.minLength(3)]),
    streetName:new FormControl("",[Validators.required,Validators.minLength(3)]),
    city:new FormControl("",[Validators.required,Validators.minLength(3)]),
    state1:new FormControl("",[Validators.required,Validators.minLength(3)]),
    district1:new FormControl("",[Validators.required,Validators.minLength(3)]),
    PINCode:new FormControl("",[Validators.required,Validators.minLength(3)]),
    telephoneNumber:new FormControl("",[Validators.required,Validators.minLength(3)]),
    emailID:new FormControl({value: this.localdata, disabled: true}),
    referenceName:new FormControl(""),
    address:new FormControl(""),
    RtelephoneNumber:new FormControl("")
  })  
  submitApplication(){
    let bodyData={
      firstName:this.applicationForm.value.firstName,
      lastName:this.applicationForm.value.lastName,
      DOB:this.applicationForm.value.DOB,
      placeOfBirth:this.applicationForm.value.placeOfBirth,
      gender:this.applicationForm.value.gender,
      state:this.applicationForm.value.state,
      district:this.applicationForm.value.district,
      maritalStatus:this.applicationForm.value.maritalStatus,
      PAN:this.applicationForm.value.PAN,
      employmentType:this.applicationForm.value.employmentType,
      highestEducation:this.applicationForm.value.highestEducation,
      fathersName:this.applicationForm.value.fathersName,
      mothersName:this.applicationForm.value.mothersName,
      houseNo:this.applicationForm.value.houseNo,
      streetName:this.applicationForm.value.streetName,
      city:this.applicationForm.value.city,
      state1:this.applicationForm.value.state1,
      district1:this.applicationForm.value.district1,
      PINCode:this.applicationForm.value.PINCode,
      telephoneNumber:this.applicationForm.value.telephoneNumber,
      emailID:localStorage.getItem('token'),
      referenceName:this.applicationForm.value.referenceName,
      address:this.applicationForm.value.address,
      RtelephoneNumber:this.applicationForm.value.RtelephoneNumber
    }
    if(bodyData.firstName.length !=0 &&bodyData.lastName.length !=0 &&bodyData.DOB.length !=0 &&bodyData.placeOfBirth.length !=0 &&bodyData.gender.length !=0 &&
      bodyData.state.length !=0 &&bodyData.district.length !=0 &&bodyData.maritalStatus.length !=0 &&bodyData.PAN.length !=0 &&bodyData.employmentType.length !=0 &&
      bodyData.highestEducation.length !=0 &&bodyData.fathersName.length !=0 &&bodyData.mothersName.length !=0 &&bodyData.houseNo.length !=0 &&bodyData.streetName.length !=0 &&
      bodyData.city.length !=0 &&bodyData.state1.length !=0 &&bodyData.district1.length !=0 &&bodyData.PINCode.length !=0 &&bodyData.telephoneNumber.length !=0 &&
      bodyData.referenceName.length !=0 || bodyData.address.length !=0 || bodyData.RtelephoneNumber.length !=0){
      this.httpClient.post('http://localhost:3000/api/user/fillapplicationform',bodyData,{ responseType: 'text' }).subscribe((res:any)=>{
      alert("data added")
      this.applicationForm.reset()
    })
    }
    else{
      alert('fill everthing and submit')
    }
  }
    

  constructor(private httpClient:HttpClient) { }

  ngOnInit(): void {
  }

}
