import { Component, OnInit } from '@angular/core';
import {FormsModule,ReactiveFormsModule} from '@angular/forms';
import { FormControl,FormGroup,Validators } from '@angular/forms';
import { HttpClient} from '@angular/common/http'
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  
  constructor(private HttpClient:HttpClient,private router:Router){}
  loginForm = new FormGroup({
    email:new FormControl("",[Validators.required,Validators.minLength(3)]),
    password:new FormControl("",[Validators.required,Validators.minLength(6)])
  })
  email(){
    return this.loginForm.get('email')
  }
  password(){
    return this.loginForm.get('password')
  }

  NameError()
  {
    if(this.email()?.invalid && (this.email()?.dirty||this.email()?.touched)){
      if(this.email()?.hasError('required'))
    {
      return "email must be filled"
    }
    else if (this.email()?.hasError('minlength'))
    {
      return "email canot be lessthan 3 charcter"
    }
    else{
      return ""
    }
    }
    else{
      return "";
    }
  }

  PasswordError()
  {
    if(this.password()?.invalid && (this.password()?.dirty||this.password()?.touched)){
      if(this.password()?.hasError('required'))
      {
        return "password must be filled"
      }
      else if (this.password()?.hasError('minlength'))
      {
        return "password canot be lessthan 6 charcter"
      }
      else{
        return ""
      }
      }
      else{
        return "";
      }
    }

    login(){
      let bodyData={
        email:this.loginForm.value.email,
        password:this.loginForm.value.password
      }
      if(bodyData.email.length !=0 && bodyData.password.length !=0){
      this.HttpClient.post('http://localhost:3000/api/user/login',bodyData,{ responseType: 'text' }).subscribe((res:any)=>{
        if(res){
          let token=this.loginForm.value.email
          localStorage.setItem('token',token)   
          this.router.navigate(['/dashboard'])
        }
      })}
      else{
        alert('filleverything')
      }
    }
    ngOnInit(): void {
    }
  
  }
