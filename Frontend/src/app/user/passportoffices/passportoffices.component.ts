import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { office } from 'src/app/model/office/office.module';
import { ServiceService } from 'src/app/service/service.service';
@Component({
  selector: 'app-passportoffices',
  templateUrl: './passportoffices.component.html',
  styleUrls: ['./passportoffices.component.css']
})
export class PassportofficesComponent implements OnInit {

  constructor(private http:HttpClient,private OfficeService:ServiceService) { 
  }
  data1:office[]=[]
  
  ngOnInit(): void {
    this.OfficeService.getOffice().subscribe(data=>{
      this.data1=data
    })
  }
}