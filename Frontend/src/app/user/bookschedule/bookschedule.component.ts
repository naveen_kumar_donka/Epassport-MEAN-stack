import { Component, OnInit } from '@angular/core';
import { schedule } from 'src/app/model/schedule/schedule.module';
import { ServiceService } from 'src/app/service/service.service';

@Component({
  selector: 'app-bookschedule',
  templateUrl: './bookschedule.component.html',
  styleUrls: ['./bookschedule.component.css']
})
export class BookscheduleComponent implements OnInit {

  bookSchedule(){
    alert('schedule is booked sucessfully wait for the conformation from admin')
  }
  constructor(private scheduleService:ServiceService) { }
  data1:schedule[]=[]
  ngOnInit(): void {
    this.scheduleService.getSchedule().subscribe(data=>{
      this.data1=data      
    })
}
}