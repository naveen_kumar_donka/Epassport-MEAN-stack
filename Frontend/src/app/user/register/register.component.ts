import { Component, OnInit } from '@angular/core';
import {FormsModule,ReactiveFormsModule} from '@angular/forms';
import { FormControl,FormGroup,Validators } from '@angular/forms';
import { HttpClient} from '@angular/common/http'
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  constructor(private HttpClient:HttpClient,private router:Router){}
  registerForm = new FormGroup({
    Firstname:new FormControl("",[Validators.required,Validators.minLength(3)]),
    Lastname:new FormControl("",[Validators.required,Validators.minLength(3)]),
    DateofBirth:new FormControl("",[Validators.required,Validators.minLength(3)]),
    Gender:new FormControl("",[Validators.required,Validators.minLength(3)]),
    email:new FormControl("",[Validators.required,Validators.email]),
    password:new FormControl("",[Validators.required,Validators.minLength(6)]),
    phoneNo:new FormControl("",[Validators.required,Validators.pattern("^((\\+91-?)|0)?[0-9]{10}$")])
    })
  
  username(){
    return this.registerForm.get('Firstname');
  }
  lastname(){
    return this.registerForm.get('Lastname')
  }
  gender(){
    return this.registerForm.get('Gender')
  }
  DateofBirth(){
    return this.registerForm.get('DateofBirth')
  }
  email(){
    return this.registerForm.get('email');
  }
  password(){
    return this.registerForm.get('password');
  }
  phone(){
    return this.registerForm.get('phoneNo')
  }
  
  NameError()
  {
    if(this.username()?.invalid && (this.username()?.dirty||this.username()?.touched)){
      if(this.username()?.hasError('required'))
    {
      return "first/last name must be filled"
    }
    else if (this.username()?.hasError('minlength'))
    {
      return "first/last name canot be lessthen 3 charcter"
    }
    else{
      return ""
    }
    }
    else{
      return "";
    }
  }
  genderError(){
    if(this.gender()?.invalid && (this.gender()?.dirty||this.gender()?.touched)){
      if(this.gender()?.hasError('required'))
      {
        return "gender must be filled"
      }
      else if (this.gender()?.hasError('minlength'))
      {
        return "gender canot be lessthen 3 charcter"
      }
      else{
        return ""
      }
      }
      else{
        return "";
      }
  }
  dobError(){
    if(this.DateofBirth()?.invalid && (this.DateofBirth()?.dirty||this.DateofBirth()?.touched)){
      if(this.DateofBirth()?.hasError('required'))
      {
        return "dob must be filled"
      }
      else if (this.DateofBirth()?.hasError('minlength'))
      {
        return "dob canot be lessthen 6 charcter"
      }
      else{
        return ""
      }
      }
      else{
        return "";
      }
  }

  PasswordError()
  {
    if(this.password()?.invalid && (this.password()?.dirty||this.password()?.touched)){
      if(this.password()?.hasError('required'))
      {
        return "password must be filled"
      }
      else if (this.password()?.hasError('minlength'))
      {
        return "password canot be lessthen 6 charcter"
      }
      else{
        return ""
      }
      }
      else{
        return "";
      }
    }

    emailError(){
      if(this.email()?.touched){
        if(this.email()?.hasError('required')){
          return "email must be filled"
        }
        else if(this.email()?.hasError('email')){
          return "it must be in the form of name@gmail.com"
        }
      }
      return ""
    }

    phoneError(){
      if(this.phone()?.touched){
      if(this.phone()?.hasError('required'))
      {
        return "phone number must be filled"
      }
      else if (this.phone()?.hasError('pattern'))
      {
        return "number canot be lessthen 10 digits number starts with 6-9"
      }
      else{
        return ""
      }
      }
      else{
        return "";
      }
  
    }
    register(){
      let bodyData ={
        Firstname:this.registerForm.value.Firstname,
        Lastname:this.registerForm.value.Lastname,
        DateofBirth:this.registerForm.value.DateofBirth,
        Gender:this.registerForm.value.Gender,
        email:this.registerForm.value.email,
        password:this.registerForm.value.password,
        phoneNo:this.registerForm.value.phoneNo
      }
      if(bodyData.Firstname.length !=0 &&
         bodyData.Lastname.length !=0 && 
         bodyData.DateofBirth.length !=0 &&
         bodyData.Gender.length !=0 &&
         bodyData.email.length !=0 &&
         bodyData.phoneNo.length !=0 &&
         bodyData.password.length !=0){
      this.HttpClient.post('http://localhost:3000/api/user/register',bodyData,{ responseType: 'text' }).subscribe((res:any)=>{
        alert("user register sucessful")
        this.router.navigate(['/login'])
      })}
      else{
        alert("fill everything")
      }
    }
    ngOnInit(): void {
    }
}