import { HttpClient } from '@angular/common/http';
import { ApplicationModule, Component, OnInit } from '@angular/core';
import { applicationform } from 'src/app/model/application/application.module';
import { ServiceService } from 'src/app/service/service.service';

@Component({
  selector: 'app-viewapplication',
  templateUrl: './viewapplication.component.html',
  styleUrls: ['./viewapplication.component.css']
})
export class ViewapplicationComponent implements OnInit {

  constructor(private httpClient:HttpClient,private applicationService:ServiceService) { }
  data1:applicationform[]=[]
  ngOnInit(): void {
    let email:any=localStorage.getItem('token')
    this.applicationService.getApplicationForm(email).subscribe((ress)=>{
      this.data1=ress     
    })
  }

}
