import { Injectable } from '@angular/core';
import { office } from '../model/office/office.module';
import { HttpClient } from '@angular/common/http';
import { users } from '../model/user/user.module';
import { schedule } from '../model/schedule/schedule.module'
import { applicationform } from '../model/application/application.module';

@Injectable({
  providedIn: 'root'
})
export class ServiceService {

  getOffice(){
    return this.HttpClient.get<office[]>('http://localhost:3000/api/admin/officedetails')
  }
  getUsers(){
    return this.HttpClient.get<users[]>('http://localhost:3000/api/admin/users')
  }
  getSchedule(){
    return this.HttpClient.get<schedule[]>('http://localhost:3000/api/admin/schedules')
  }
  getApplicationForm(email:String){
    return this.HttpClient.get<applicationform[]>(`http://localhost:3000/api/user/viewapplicationform/${email}`)
  }
  loaduser(email:string){
    return this.HttpClient.get<users[]>(`http://localhost:3000/api/user/loaduser/${email}`)
  }
  getOneUser(id:string){
    return this.HttpClient.get<users[]>(`http://localhost:3000/api/admin/user/${id}`)
  }
  getOneSchedule(id:string){
    return this.HttpClient.get<schedule[]>(`http://localhost:3000/api/admin/oneschedule/${id}`)
  }
  getAllApplications(){
    return this.HttpClient.get<applicationform[]>('http://localhost:3000/api/admin/applications')
  }
  getOneApplication(id:string){
    return this.HttpClient.get<applicationform[]>(`http://localhost:3000/api/admin/oneApplicationdata/${id}`)
  }
  getOneOffice(id:string){
    return this.HttpClient.get<office[]>(`http://localhost:3000/api/admin/oneoffice/${id}`)
  }
  constructor(private HttpClient:HttpClient) { }
}
