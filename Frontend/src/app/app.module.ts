import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './user/login/login.component';
import { HeaderComponent } from './user/header/header.component';
import { ViewapplicationComponent } from './user/viewapplication/viewapplication.component';
import { RegisterComponent } from './user/register/register.component';
import { PassportofficesComponent } from './user/passportoffices/passportoffices.component';
import { DefaultComponent } from './user/default/default.component';
import { BookscheduleComponent } from './user/bookschedule/bookschedule.component';
import { ApplicationformComponent } from './user/applicationform/applicationform.component';
import { DashboardComponent } from './user/dashboard/dashboard.component';
import { AdminloginComponent } from './admin/adminlogin/adminlogin.component';
import { AdminDashboardComponent } from './admin/admin-dashboard/admin-dashboard.component';
import { AdminHeaderComponent } from './admin/admin-header/admin-header.component';
import { AdduserComponent } from './admin/adduser/adduser.component';
import { AddofficeComponent } from './admin/addoffice/addoffice.component';
import { AddScheduleComponent } from './admin/add-schedule/add-schedule.component';
import { ViewusersComponent } from './admin/viewusers/viewusers.component';
import { ViewofficesComponent } from './admin/viewoffices/viewoffices.component';
import { ViewoscheduleComponent } from './admin/viewoschedule/viewoschedule.component';
import { PassportstausComponent } from './user/passportstaus/passportstaus.component';
import { ViewfilledapplicationsComponent } from './admin/viewfilledapplications/viewfilledapplications.component';
import { UserUpdateFormComponent } from './admin/viewusers/user-update-form/user-update-form.component';
import { OfficeUpdateFormComponent } from './admin/viewoffices/office-update-form/office-update-form.component';
import { ScheduleUpdateFormComponent } from './admin/viewoschedule/schedule-update-form/schedule-update-form.component';


import {MatFormFieldModule} from '@angular/material/form-field';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {FormsModule,ReactiveFormsModule} from '@angular/forms';
import {MatMenuModule} from '@angular/material/menu';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatInputModule} from '@angular/material/input';
import {MatDividerModule} from '@angular/material/divider';
import {MatButtonToggleModule} from '@angular/material/button-toggle';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';
import { ServiceService } from './service/service.service';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HeaderComponent,
    ViewapplicationComponent,
    RegisterComponent,
    PassportofficesComponent,
    DefaultComponent,
    BookscheduleComponent,
    ApplicationformComponent,
    DashboardComponent,
    AdminloginComponent,
    AdminDashboardComponent,
    AdminHeaderComponent,
    AdduserComponent,
    AddofficeComponent,
    AddScheduleComponent,
    ViewusersComponent,
    ViewofficesComponent,
    ViewoscheduleComponent,
    PassportstausComponent,
    ViewfilledapplicationsComponent,
    UserUpdateFormComponent,
    OfficeUpdateFormComponent,
    ScheduleUpdateFormComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,MatIconModule,MatMenuModule,MatDatepickerModule,
    BrowserAnimationsModule,MatButtonModule,MatFormFieldModule,FormsModule,ReactiveFormsModule,HttpClientModule,BrowserAnimationsModule,MatInputModule,MatDividerModule,MatButtonToggleModule

  ],
  providers: [ServiceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
